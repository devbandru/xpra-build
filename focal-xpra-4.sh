#!/bin/bash

set -ex

# CLI Args
while [ "$#" -gt 0 ]; do
    case "${1}" in
        -s|--split) BUILD_TYPE="split"; ;;
        -u|--unified) BUILD_TYPE="unified" shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

DATE="$(date +%Y-%m-%d-%H%M%S)"; export DATE
export IMAGE_NAME="xpra-4-beta-focal-${DATE}"
export XZ_OPT="-1 -T 0"
export OS_USERNAME="xpra"
export OS_USERPASS="password"
BASE_DIR="$(pwd)"; export BASE_DIR
export BUILD_DIR="build"
export ROOTFS_DIR="${BUILD_DIR}/rootfs"
export TEMP_DIR="temp"

if [ -z "${BUILD_TYPE}" ]
then
  BUILD_TYPE="unified"
fi

# Clear build dir
purge_build_dir() {
  echo "Deleting files from build env..."
  rm -rf --one-file-system "${BUILD_DIR}"  || true
}

prepare_dirs() {
  mkdir -p "${BUILD_DIR}" temp
}

install_host_pkgs() {
  apt -y update
  apt -y install debootstrap squashfs-tools zstd
}

# Build minimal ubuntu
build_ubuntu() {
  echo "Debootstrapping the minimum Ubuntu Focal..."
  debootstrap --include=wget,gnupg focal "${ROOTFS_DIR}" || exit 1
}

# Prepare apt sources
prepare_apt_sources() {
  mkdir -p "${ROOTFS_DIR}"/etc/apt/
  cp /etc/apt/sources.list "${ROOTFS_DIR}"/etc/apt/
  # Install Xpra Beta repo
  chroot "${ROOTFS_DIR}" /bin/bash -c "wget -q https://xpra.org/gpg.asc -O- | apt-key add -" || exit 1
  curl https://xpra.org/repos/focal/xpra-beta.list > "${ROOTFS_DIR}"/etc/apt/sources.list.d/xpra-beta.list || exit 1
  # Install Wine repo
  chroot "${ROOTFS_DIR}" dpkg --add-architecture i386
  chroot "${ROOTFS_DIR}" /bin/bash -c "wget -q https://dl.winehq.org/wine-builds/winehq.key -O- | apt-key add -" || exit 1
  echo 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main' > "${ROOTFS_DIR}"/etc/apt/sources.list.d/wine.list
}

mount_system_dirs() {
  for virt_fs in {dev,proc,run,sys}
  do
    mount --bind /"${virt_fs}" "${ROOTFS_DIR}/${virt_fs}"
  done
}

install_chroot_pkgs() {
  chroot "${ROOTFS_DIR}" dpkg-reconfigure debconf -f noninteractive -p critical
  chroot "${ROOTFS_DIR}" apt -y update
  chroot "${ROOTFS_DIR}" apt -y full-upgrade
  chroot "${ROOTFS_DIR}" apt -y install \
                         openssh-server \
                         pulseaudio pulseaudio-utils \
                         python3-opencv python-psutil python3-lzo python3-openssl python3-pyinotify python3-setuptools
  chroot "${ROOTFS_DIR}" apt -y install --no-install-recommends xserver-xorg xserver-xorg-video-dummy
  chroot "${ROOTFS_DIR}" apt -y install xpra xpra-html5

  for gui_app in $(cat files/gui_app_list)
  do
    chroot "${ROOTFS_DIR}" apt -y install "${gui_app}"
  done
}

copy_xpra_config() {
  cp -fr files/xpra/config/* "${ROOTFS_DIR}"/etc/xpra/conf.d/
}

set_xpra_systemd_unit() {
  cp -fr files/xpra/systemd/xpra-server.service "${ROOTFS_DIR}"/etc/systemd/system/
  cp -fr files/xpra/systemd/xpra-server.env "${ROOTFS_DIR}"/etc/default/xpra-server
  chroot "${ROOTFS_DIR}" systemctl disable xpra.service xpra.socket
  chroot "${ROOTFS_DIR}" systemctl enable xpra-server.service
}

set_os_env() {
  chroot "${ROOTFS_DIR}" bash -c "useradd -m ${OS_USERNAME} -p ${OS_USERPASS} -u 4242 -g xpra -s /bin/bash"
  cp files/network/netplan/00-default.yaml "${ROOTFS_DIR}"/etc/netplan/
  chroot "${ROOTFS_DIR}" netplan generate
}

umount_all_in_build_env() {
  umount -q --recursive "${ROOTFS_DIR}"/* || true
}

delete_pkgs() {
  for pkg in $(cat files/delete_pkgs_list)
  do
    chroot "${ROOTFS_DIR}" apt -y purge "${pkg}"
  done
}

clear_build_env() {
  chroot "${ROOTFS_DIR}" apt -y autoremove --purge
  chroot "${ROOTFS_DIR}" apt -y clean

  for dir in $(cat files/delete_files_list)
  do
    echo "Deleting ${dir} dir files from build env..."
    rm -rfv "${ROOTFS_DIR:?}"/"${dir:?}"
  done
}

create_archive_new() {
  mkdir -p release/"${DATE}"
  sync

  if [ "${BUILD_TYPE}" = "split" ]
  then
    mksquashfs "${ROOTFS_DIR}" release/"${DATE}"/"${IMAGE_NAME}".squashfs -comp zstd -Xcompression-level 1
    tar -ca \
        -f release/"${DATE}"/metadata.tar.lz4 \
        files/metadata.yaml
    cp files/metadata.yaml release/"${DATE}"
    sha512sum release/"${DATE}"/"${IMAGE_NAME}".squashfs > release/"${DATE}"/rootfs.sha512sum
    sha512sum release/"${DATE}"/metadata.tar.zst > release/"${DATE}"/metadata.sha512sum

  elif [ "${BUILD_TYPE}" = "unified" ]
  then
    cp files/metadata.yaml "${BUILD_DIR}"
    tar -ca \
        --exclude-from files/delete_files_list \
        -f release/"${DATE}"/"${IMAGE_NAME}".tar.lz4 \
        -C "${BUILD_DIR}" \
        --one-file-system \
        --xattrs \
        .
  sha512sum release/"${DATE}"/"${IMAGE_NAME}".tar.lz4 > release/"${DATE}"/unified.sha512sum
  fi
}

create_archive() {
  sync
  tar -cpJ \
      --exclude-from exclude_list_for_tar \
      -f "${IMAGE_NAME}".tar.xz \
      -C  \
      --one-file-system \
      --xattrs \
      .

  mkdir -p release/"${DATE}"
  mv "$IMAGE_NAME".tar.xz release/"${DATE}"
  cp metadata.yaml release/"${DATE}"/
  tar -cpJf release/"${DATE}"/metadata.tar.gz -C release/"${DATE}" metadata.yaml
  rm release/"${DATE}"/metadata.yaml
}

create_qcow_image() {
  FILESIZE=$(du -b --max-depth=0 build/ | cut -f1)
  FILESIZE=$(( FILESIZE * 1.5 ))
  mkdir -p release/"${DATE}"/
  truncate -s "${FILESIZE}" release/"${DATE}"/"$IMAGE_NAME".raw
  mkfs.xfs -f release/"${DATE}"/"$IMAGE_NAME".raw
  mkdir -p /tmp/lxd-raw-mount
  mount -o loop release/"${DATE}"/"$IMAGE_NAME".raw /tmp/lxd-raw-mount
  cp -rpa build/* /tmp/lxd-raw-mount
  umount /tmp/lxd-raw-mount
  qemu-img convert -f raw -O qcow2 \
           release/"${DATE}"/"${IMAGE_NAME}".raw \
           release/"${DATE}"/"${IMAGE_NAME}".qcow2
  tar -cpJf release/"${DATE}"/"${IMAGE_NAME}"-qcow2.tar.xz \
      -C release/"${DATE}" "${IMAGE_NAME}".qcow2
  rm -rf release/"${DATE}"/"${IMAGE_NAME}".raw \
         release/"${DATE}"/"${IMAGE_NAME}".qcow2
}

delete_existing_lxd_image() {
  lxc image delete "${IMAGE_NAME}"-"${DATE}" || true
}

import_image_to_lxd() {
  lxc image import \
            release/"${DATE}"/metadata.tar.gz \
            release/"${DATE}"/"${IMAGE_NAME}".tar.xz \
            --alias "${IMAGE_NAME}"-latest
}

recreate_existing_lxd_cont() {
  lxc delete "${IMAGE_NAME}" --force || true
  lxc launch "${IMAGE_NAME}"-"${DATE}" "${IMAGE_NAME}"
}


prepare_to_build() {
  umount_all_in_build_env
  purge_build_dir
  prepare_dirs
  install_host_pkgs
  build_ubuntu
  prepare_apt_sources
  mount_system_dirs
}

build_images() {
  install_chroot_pkgs
  copy_xpra_config
  set_xpra_systemd_unit
  set_os_env
  delete_pkgs
  clear_build_env
  umount_all_in_build_env
  create_archive_new
#  create_qcow_image
}

add_archive_to_lxd() {
  delete_existing_lxd_image
  import_image_to_lxd
#  recreate_existing_lxd_cont
}

main() {
  prepare_to_build
  build_images
#  add_archive_to_lxd
}

test() {
  create_qcow_image
}

# Main function
main

# Test function
#test

