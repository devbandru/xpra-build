#!/bin/bash

BUILD="rpi4"
export OS="debian"
OSVER=$(cat /etc/debian_version)
export OSVER
CPUNUM=$(nproc)
export CPUNUM
export DEB_BUILD_OPTIONS="parallel=$CPUNUM"
export CCACHE_DIR="$HOME/xpra/xpra-ccache"

#COMPILEMODE="$2"
#export COMPILEMODE

#DEB_CFLAGS=$(dpkg-buildflags --get CFLAGS)

#if [[ -z "${COMPILEMODE}" ]]
#then
#  export COMPILEMODE="native"
#fi

#if [[ "${COMPILEMODE}" = "native" ]]
#then
#  export DEB_CFLAGS_SET="$DEB_CFLAGS -march=native -mtune=native -ftree-vectorize"
#fi
#if [[ "${COMPILEMODE}" = "native-lto" ]]
#then
#  export DEB_CFLAGS_SET="-march=native -mtune=native -flto=$CPUNUM"
#elif [[ "$COMPILEMODE" = "fast" ]]
#then
#  export DEB_CFLAGS_SET="-march=native -mtune=native -O3 -funroll-loops"
#elif [[ "$COMPILEMODE" = "fast-lto" ]]
#then
#  export DEB_CFLAGS_SET="-march=native -mtune=native -O3 -flto=$CPUNUM -funroll-loops"
#fi

# Install build deps
sudo apt -y update
sudo apt -y dist-upgrade

sudo apt -y install libx11-dev libxtst-dev libxcomposite-dev libxdamage-dev libxkbfile-dev python-all-dev \
                 libgtk-3-dev python3-dev python3-cairo-dev python-gi-dev cython3 \
                 xauth x11-xkb-utils \
                 libx264-dev libvpx-dev yasm \
                 libavformat-dev libavcodec-dev libswscale-dev \
                 libturbojpeg-dev \
                 libwebp-dev \
                 node-uglify brotli python3-brotli libjs-jquery libjs-jquery-ui gnome-backgrounds \
                 python3-rencode python3-lz4 python3-dbus python3-cryptography python3-netifaces python3-yaml python3-lzo \
                 python3-setproctitle python3-xdg python3-pyinotify python3-opencv \
                 libpam-dev quilt xserver-xorg-dev xutils-dev xserver-xorg-video-dummy xvfb keyboard-configuration \
                 python-kerberos python-gssapi \
                 gstreamer1.0-pulseaudio gstreamer1.0-alsa gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly python-gst-1.0 \
                 cups-filters cups-common cups-pdf python3-cups \
                 openssh-client sshpass python3-paramiko \
                 python3-gi-cairo

# Install build scripts
sudo apt -y install devscripts build-essential lintian debhelper subversion python3-pip ccache

# Install latest pyopengl
pip3 install --upgrade pyopengl pyopengl-accelerate

mkdir -p "$HOME"/xpra
cd "$HOME"/xpra || exit 1
mkdir -p "${CCACHE_DIR}"

if [[ ! -e "${CCACHE_DIR}"/ccache.conf ]]
then
  ccache --max-size=2G
fi

# Get Xpra sources
svn co https://xpra.org/svn/Xpra

if [[ "$1" = "trunk" ]] || [[ -z "$1" ]]
then
  cd Xpra/trunk/src || exit 1
else
  cd Xpra/tags/v"$1"/src || bash -c 'echo "Incorrect tag, expected: MAJOR_VERSION.0.x. Exit." 1>&2; exit 1'
fi

ln -sf ../debian .
rm -f debian/xpra-html5.install
rm -f debian/python2-xpra.install
sed -i "/cuda/d" debian/xpra.install

# Build!
debuild --preserve-envvar=CCACHE_DIR --prepend-path=/usr/lib/ccache -us -uc -b -j"${CPUNUM}" -rfakeroot
mkdir -p "$HOME"/xpra/packages/"${BUILD}"-"${OS}"-"${OSVER}"-"${COMPILEMODE}"
mv ../*.deb "$HOME"/xpra/packages/"${BUILD}"-"${OS}"-"${OSVER}"-"${COMPILEMODE}"
exit 0
